import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated } from 'react-native';

import Splash from './components/Splash'
import Login from './components/Login'

class Main extends Component {

  constructor(props){
    super(props)
    this.state = { currentScreen: 'Splash', fadeAnim: new Animated.Value(1.0) }
    setTimeout(()=>{
      Animated.timing(
        this.state.fadeAnim,
        {
          toValue: 0,
          duration: 1000,
        }
      ).start();
      setTimeout(()=>{
        this.setState({ currentScreen: 'Login' })
      }, 1000)
    }, 3000)
  }

  render(){
    const { currentScreen } = this.state
    let mainScreen = currentScreen === 'Splash' ? <Animated.View style={[{opacity: this.state.fadeAnim}, styles.container]}><Splash/></Animated.View> : <Login/> 
    return(mainScreen)
  }

}

export default function App() {
  return (
    <Main/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
