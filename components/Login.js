import React, {
    Component
} from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    StatusBar,
    TextInput,
    SafeAreaView,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
    Image,
    Animated
} from 'react-native'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = { fadeAnim: new Animated.Value(0), user: "", pass: "" }
        Animated.timing(
            this.state.fadeAnim,
            {
              toValue: 1,
              duration: 500,
            }
          ).start();
    }
    login=()=>{
        const user = this.state.user
        const pass = this.state.pass
        
    }
    render() {
        return(
            <Animated.View style={[styles.container, {opacity: this.state.fadeAnim, marginTop: 20}]}>
                <SafeAreaView style={styles.container}>
                    <StatusBar backgroundColor='#0655DA' barStyle='dark-content'/>
                    <KeyboardAvoidingView behavior='padding' style={[styles.container]}>
                        <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                            <View style={styles.container}>
                                <View style={styles.logoContainer}>
                                    <Image style={styles.logo} source={require('../assets/login_image.png')}></Image>
                                    <Text style={styles.title}>Login example</Text>
                                </View>
                                <View style={styles.inputs}>
                                    <TextInput style={styles.input}
                                    placeholder='Nombre de usuario'
                                    placeholderTextColor='#00000055'
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    autoCorrect={false}
                                    onChangeText={(text)=>{this.setState({user: text})}}
                                    onSubmitEditing={()=>{this.refs.txtPassword.focus()}}/>
                                    <TextInput style={styles.input}
                                    placeholder='Contraseña'
                                    placeholderTextColor='#00000055'
                                    secureTextEntry={true}
                                    returnKeyType='go'
                                    onChangeText={(text)=>{this.setState({pass: text})}}
                                    ref={'txtPassword'}/>
                                    <TouchableOpacity onPress={this.login} style={styles.button}>
                                        <Text style={styles.buttonText}>Iniciar sesión</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logo:{
        width: 100,
        height: 50,
        marginTop: -100
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold',
        color: '#0655DA',
        marginTop: 10
    },
    inputs:{
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 200,
        padding: 20
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(0,0,0,0.1)',
        borderRadius: 20,
        paddingHorizontal: 20,
        marginBottom: 20,
        color: '#0655DA'
    },
    button:{
        backgroundColor: '#0655DA',
        borderRadius: 20,
        height: 40
    },
    buttonText:{
        color: 'white',
        textAlign: 'center',
        padding: 10,
    }
})