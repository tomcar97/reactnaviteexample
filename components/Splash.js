import React, { Component } from 'react'
import { StyleSheet, Image, View} from 'react-native'

export default class Splash extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../assets/canvas.png')}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo:{
        height: 150,
        width: 150
    }
})